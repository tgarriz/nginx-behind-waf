# Nginx Behind Waf

server {
    listen 80;
    server_name tu_dominio.com;  # Cambia esto al dominio de tu servidor

    location / {
        proxy_pass http://127.0.0.1:80;  # Proxy hacia el frontend
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $http_x_forwarded_proto;  # Si WAF añade esta cabecera
    }

    location /api/ {
        proxy_pass http://127.0.0.1:3000;  # Proxy hacia el backend API
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $http_x_forwarded_proto;  # Si WAF añade esta cabecera
    }
}